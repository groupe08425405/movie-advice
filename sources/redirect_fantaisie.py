
import cgi
import urllib.parse

print("Content-type: text/html; charset=UTF-8")
print()

# Récupération des données postées par le formulaire
form = cgi.FieldStorage()

# Extraction des choix de l'utilisateur
fantaisie1 = form.getvalue('fantaisie1')
fantaisie2 = form.getvalue('fantaisie2')
fantaisie3 = form.getvalue('fantaisie3')

# Déterminer l'ancre en fonction des choix de l'utilisateur
try:
    if fantaisie1 == 'fantaisie' and fantaisie2 == 'concept1' and fantaisie3 == 'animation': # Mon voisin Totoro
        ancre = 'ancre_1'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept2' and fantaisie3 == 'animation': # Le Château Ambulant
        ancre = 'ancre_2'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept3' and fantaisie3 == 'animation': # Le Prince d'Égypte
        ancre = 'ancre_3'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept4' and fantaisie3 == 'animation': # SHREK
        ancre = 'ancre_4'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept1' and fantaisie3 == 'joue': # LE HOBBIT
        ancre = 'ancre_5'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept2' and fantaisie3 == 'joue': # HARRY POTTER
        ancre = 'ancre_6'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept3' and fantaisie3 == 'joue': # PIRATES DES CARAIBES
        ancre = 'ancre_7'
    elif fantaisie1 == 'fantaisie' and fantaisie2 == 'concept4' and fantaisie3 == 'joue': # Le Livre de la jungle
        ancre = 'ancre_8'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept1' and fantaisie3 == 'animation': # Le Royaume des chats
        ancre = 'ancre_9'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept2' and fantaisie3 == 'animation': # L'étrange Noël de Monsieur Jack
        ancre = 'ancre_10'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept3' and fantaisie3 == 'animation': # anastasia
        ancre = 'ancre_11'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept4' and fantaisie3 == 'animation': # Dorothy et le Magicien d'Oz
        ancre = 'ancre_12'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept1' and fantaisie3 == 'joue': # Le Labyrinthe de Pan
        ancre = 'ancre_13'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept2' and fantaisie3 == 'joue': # L'étrange histoire de Benjamin Button
        ancre = 'ancre_14'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept3' and fantaisie3 == 'joue': # X MEN
        ancre = 'ancre_15'
    elif fantaisie1 == 'fantastique' and fantaisie2 == 'concept4' and fantaisie3 == 'joue': # LA BELLE ET LA BÊTE
        ancre = 'ancre_16'
    else:
        raise Exception
except:
    if fantaisie1 is None or fantaisie2 is None or fantaisie3 is None:
        print("<h1>Revenez en arrière, il vous faut répondre à toutes les questions.</h1>")
else:
    # Construire l'URL de redirection vers la troisième page avec les paramètres du formulaire et l'ancre appropriée
    redirect_url = f"conseil_fantaisie.py?fantaisie1={fantaisie1}&fantaisie2={fantaisie2}&fantaisie3={fantaisie3}#{ancre}"


#Rediriger l'utilisateur
print(f"<script>window.location.href = '{redirect_url}';</script>")
