
import cgi
import urllib.parse

print("Content-type: text/html; charset=UTF-8")
print()

# Récupération des données postées par le formulaire
form = cgi.FieldStorage()

# Extraction des choix de l'utilisateur
science1 = form.getvalue('science1')
science2 = form.getvalue('science2')
science3 = form.getvalue('science3')

# Déterminer l'ancre en fonction des choix de l'utilisateur
try:
    if science1 == 'lieu1' and science2 == 'concept1' and science3 == 'populaire': # STARWARS
        ancre = 'ancre_1'
    elif science1 == 'lieu1' and science2 == 'concept1' and science3 == 'impopulaire': # OUTLAND
        ancre = 'ancre_2'
    elif science1 == 'lieu1' and science2 == 'concept2' and science3 == 'populaire': # LES GARDIENS DE LA GALAXIE
        ancre = 'ancre_3'
    elif science1 == 'lieu1' and science2 == 'concept2' and science3 == 'impopulaire': # VALERIAN ET LA CITE DES MILLE PLANETES
        ancre = 'ancre_4'
    elif science1 == 'lieu1' and science2 == 'concept3' and science3 == 'populaire': # ALIEN, LE HUITIEME PASSAGER
        ancre = 'ancre_5'
    elif science1 == 'lieu1' and science2 == 'concept3' and science3 == 'impopulaire': # DISTRICT 9
        ancre = 'ancre_6'
    elif science1 == 'lieu1' and science2 == 'concept4' and science3 == 'populaire': # AVATAR
        ancre = 'ancre_7'
    elif science1 == 'lieu1' and science2 == 'concept4' and science3 == 'impopulaire': # MOON
        ancre = 'ancre_8'
    elif science1 == 'lieu2' and science2 == 'concept1' and science3 == 'populaire': # STARWARS
        ancre = 'ancre_1'
    elif science1 == 'lieu2' and science2 == 'concept1' and science3 == 'impopulaire': # PLANETE ROUGE
        ancre = 'ancre_10'
    elif science1 == 'lieu2' and science2 == 'concept2' and science3 == 'populaire': # THOR: RAGNAROK
        ancre = 'ancre_11'
    elif science1 == 'lieu2' and science2 == 'concept2' and science3 == 'impopulaire': # JUSPITER: LE DESTIN DE L'UNIVERS
        ancre = 'ancre_12'
    elif science1 == 'lieu2' and science2 == 'concept3' and science3 == 'populaire': # AVATAR
        ancre = 'ancre_7'
    elif science1 == 'lieu2' and science2 == 'concept3' and science3 == 'impopulaire': # PITCH BLACK
        ancre = 'ancre_14'
    elif science1 == 'lieu2' and science2 == 'concept4' and science3 == 'populaire': # INTERSTELLAR
        ancre = 'ancre_15'
    elif science1 == 'lieu2' and science2 == 'concept4' and science3 == 'impopulaire': # SEUL SUR MARS
        ancre = 'ancre_16'
    elif science1 == 'lieu3' and science2 == 'concept1' and science3 == 'populaire': # TERMINATOR
        ancre = 'ancre_17'
    elif science1 == 'lieu3' and science2 == 'concept1' and science3 == 'impopulaire': # UPGRADE
        ancre = 'ancre_18'
    elif science1 == 'lieu3' and science2 == 'concept2' and science3 == 'populaire': # MAN OF STEEL
        ancre = 'ancre_19'
    elif science1 == 'lieu3' and science2 == 'concept2' and science3 == 'impopulaire': # CHRONICLE
        ancre = 'ancre_20'
    elif science1 == 'lieu3' and science2 == 'concept3' and science3 == 'populaire': # LA GUERRE DES MONDES
        ancre = 'ancre_21'
    elif science1 == 'lieu3' and science2 == 'concept3' and science3 == 'impopulaire':# THE DARKEST HOUR
        ancre = 'ancre_22'
    elif science1 == 'lieu3' and science2 == 'concept4' and science3 == 'populaire': # INTERSTELLAR
        ancre = 'ancre_15'
    elif science1 == 'lieu3' and science2 == 'concept4' and science3 == 'impopulaire': # JOHN CARTER
        ancre = 'ancre_24'
    else:
        raise Exception
except:
    if science1 is None or science2 is None or science3 is None:
        print("<h1>Revenez en arrière, il vous faut répondre à toutes les questions.</h1>")
else:
    # Construire l'URL de redirection vers la troisième page avec les paramètres du formulaire et l'ancre appropriée
    redirect_url = f"conseil_sci-fiction.py?science1={science1}&science2={science2}&science3={science3}#{ancre}"


#Rediriger l'utilisateur
print(f"<script>window.location.href = '{redirect_url}';</script>")


