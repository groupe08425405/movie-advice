# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 16:35:44 2024

@author: herve
"""

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> MovieAdvice Action </title>
    <link rel="stylesheet" href="Style/style_action.css" >
</head>
<body>
     
   <div class="content">
    <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div>
     
    <div class="container">
      <div class="text-container">
        <p class="resume">
          "Salt" est un film d'action captivant mettant en vedette Angelina Jolie dans le rôle d'une agente de la CIA accusée à tort d'être une espionne russe. Pour prouver son innocence, elle doit échapper à ses collègues et démêler un complot international complexe. Le film est rempli de scènes d'action spectaculaires et de retournements de situation inattendus, promettant une expérience palpitante pour les amateurs de suspense et d'adrénaline.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=130203.html"><em>SALT</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_1" src="images/Salt.jpg" class="Salt_picture" alt="affiche du film Salt">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          "Baby Driver" est un film d'action dirigé par Edgar Wright. On suit l'histoire de Baby, un chauffeur doué qui travaille pour rembourser une dette en conduisant pour un criminel. Avec ses acouphènes, il trouve réconfort dans la musique. Quand il rencontre Debora, l'amour le pousse à quitter le crime, mais Doc l'oblige à participer à un dernier braquage. Le film est salué pour son rythme entraînant, sa bande-son percutante et ses scènes d'action spectaculaires.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=230453.html"><em>BABY DRIVER</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_2" src="images/baby_driver.jpg" class="baby_driver" alt="affiche du film Baby Driver">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          "Mad Max: Fury Road" est un film d'action post-apocalyptique qui se déroule dans un désert aride où règne la loi du plus fort. Max, un ancien policier hanté par son passé, se retrouve capturé par un groupe dirigé par Immortan Joe. Furiosa, une guerrière rebelle, s'échappe avec les épouses de Joe, déclenchant une poursuite effrénée à travers le désert. Max se joint à elle dans une course contre la mort pour atteindre un endroit sûr. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=125054.html"><em>MAD MAX</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_3" src="images/mad_max.jpg" class="mad_max" alt="affiche du film Mad Max: Fury Road">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Terminator", un cyborg indestructible, envoyé du futur, traque Sarah Connor, cible d'un assassinat destiné à altérer le cours de l'histoire. Avec l'aide de Kyle Reese, un soldat du futur, Sarah lutte pour sa survie, réalisant qu'elle détient le destin de l'humanité entre ses mains. Le film offre une combinaison captivante d'action, de suspense et de science-fiction, explorant les thèmes du destin et de la résistance contre la technologie.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=309.html"><em>TERMINATOR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_4" src="images/terminator.jpg" class="terminator" alt="affiche du film Terminator">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Colombiana", Cataleya Restrepo, une jeune femme devenue tueuse à gages après le meurtre de ses parents par des criminels, poursuit sa quête de vengeance à travers le monde. Avec une détermination implacable et des compétences redoutables, elle cherche à éliminer ceux qui ont détruit sa famille. Dans un jeu mortel de cat-and-mouse, Cataleya défie les puissants barons de la drogue et les autorités pour obtenir justice.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=182013.html"><em>COLOMBIANA</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_5" src="images/colombiana.jpg" class="terminator" alt="affiche du film Colombiana">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
          Dans "Rocky", on suit l'histoire de Rocky Balboa, un boxeur de Philadelphie issu d'un quartier défavorisé, qui voit sa vie changer lorsqu'il se voit offrir la chance de combattre le champion du monde de boxe poids lourd. Sous la direction de son entraîneur, Mickey, Rocky se prépare pour le combat de sa vie. Ce film emblématique, réalisé par Sylvester Stallone lui-même, raconte une histoire inspirante de détermination, de lutte et d'espoir.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=28251.html"><em>ROCKY</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_6" src="images/rocky.jpg" class="Salt_picture" alt="affiche du film Rocky Balboa">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         Une jeune femme nommée "Lucy", se retrouve accidentellement exposée à une drogue expérimentale qui débloque la totalité de ses capacités cérébrales. Alors que ses capacités intellectuelles augmentent de manière exponentielle, Lucy devient de plus en plus puissante et cherche à comprendre sa transformation tout en échappant à ceux qui la traquent. À mesure que son esprit évolue vers des niveaux inimaginables, Lucy se lance dans une quête de connaissance et de liberté, défiant les limites de l'existence humaine.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=186452.html"><em>LUCY</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_7" src="images/lucy.jpg" class="Salt_picture" alt="affiche du film Lucy">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
         Dans "The Dark Knight", Batman se trouve confronté à son plus redoutable ennemi, le Joker, qui sème le chaos à Gotham City. Alors que le Joker défie la justice et met à l'épreuve les limites morales de Batman, le Chevalier Noir doit faire face à des choix déchirants pour protéger la ville. Avec l'aide du commissaire Gordon et du procureur Harvey Dent, Batman cherche à arrêter le Joker tout en préservant son héritage de justice. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=115362.html"><em>THE DARK KNIGHT</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_8" src="images/dark_knight.jpg" class="batman" alt="affiche du film The Dark Knight">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         Dans "Peppermint", Riley North, une femme dévastée par le meurtre de sa famille, se transforme en une redoutable justicière pour traquer ceux responsables de sa perte. Rejetant le système judiciaire corrompu, elle prend les armes pour venger sa famille et nettoyer les rues de Los Angeles. Avec une détermination implacable et des compétences meurtrières, Riley se lance dans une quête de vengeance impitoyable. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=256044.html"><em>PEPPERMINT</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_9" src="images/peppermint.jpg" class="terminator" alt="affiche du film Peppermint">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
        Dans "Fast and Furious", le film qui a lancé une saga légendaire, Dominic Toretto, un pilote de rue hors pair, règne sur les courses clandestines de Los Angeles avec son équipe. L'arrivée d'un policier infiltré, Brian O'Conner, met en péril leur monde lorsque des tensions montent entre la loi et la criminalité. Alors que Brian gagne la confiance de Dom, il est confronté à un choix impossible entre son devoir et sa loyauté. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=221541.html"><em>FAST AND FURIOUS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_10" src="images/fast&furious.jpg" class="batman" alt="affiche du film Fast And Furious">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
        Dans "Edge of Tomorrow", Rita Vrataski, une guerrière d'élite dans un monde envahi par des extraterrestres. Tom Cruise joue le rôle d'un officier qui se retrouve pris dans une boucle temporelle, revivant sans cesse la même journée de bataille. Rita, ayant elle-même vécu cette expérience, aide le protagoniste à devenir un combattant redoutable. Ensemble, ils tentent de trouver un moyen de vaincre les envahisseurs et de briser le cycle infernal.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=185030.html"><em>EDGE OF TOMORROW</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_11" src="images/EdgeOfTomorrow.jpg" class="edge" alt="affiche du film Edge Of Tomorrow">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
        "Kill Bill", une ancienne tueuse à gages connue sous le nom de "La Mariée", interprétée par Uma Thurman, cherche à se venger de son ancien employeur, Bill, et de son équipe de tueurs qui l'ont trahie et laissée pour morte lors de son mariage. À travers un voyage sanglant et implacable, elle traque ses anciens ennemis un par un pour assouvir sa soif de vengeance. Ce film d'action réalisé par Quentin Tarantino, est célèbre pour ses combats stylisés et son hommage au cinéma d'arts martiaux.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=28541.html"><em>KILL BILL</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_13" src="images/kill_bill.jpg" class="terminator" alt="affiche du film Kill Bill">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
        Dans "Mission Impossible", Ethan Hunt, un agent secret de l'IMF, se retrouve impliqué dans une série de missions impossibles pour déjouer des complots internationaux et sauver le monde. Avec son équipe d'experts, Hunt affronte des ennemis redoutables et des situations périlleuses, utilisant son ingéniosité et ses compétences en infiltration pour réussir. Ce film d'action est inspiré de la célèbre série télévisée et est rempli de cascades spectaculaires.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=147454.html"><em>MISSION IMPOSSIBLE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_14" src="images/mission_impossible.jpg" class="mission_impo" alt="affiche du film Mission Impossible">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
        Dans "Tomb Raider", Angelina Jolie incarne Lara Croft, une aventurière intrépide et experte en arts martiaux, déterminée à percer les mystères des anciennes civilisations. Sur les traces de son père disparu, Lara se lance dans une quête dangereuse pour retrouver un artefact mythique avant une organisation maléfique. Avec son courage et son intelligence, elle affronte des pièges mortels, des ennemis redoutables et des énigmes complexes, révélant ainsi son héritage et sa destinée de protectrice du monde.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27429.html"><em>TOMB RAIDER</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_15" src="images/tomb_raiderAng.jpg" class="mission_impo" alt="affiche du film Tomb Raider">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
        Dans "Matrix", Keanu Reeves incarne Neo, un jeune homme qui découvre que le monde dans lequel il vit est une simulation contrôlée par des machines intelligentes. Guidé par Morpheus, il plonge dans la réalité virtuelle et apprend qu'il est l'élu destiné à libérer l'humanité de l'emprise des machines. Neo se lance alors dans une quête périlleuse pour maîtriser ses pouvoirs et affronter l'agent Smith, un programme informatique déterminé à le détruire.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=19776.html"><em>MATRIX</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_16" src="images/matrix.jpg" class="terminator" alt="affiche du film Matrix">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Atomic Blond" Lorraine Broughton, une des meilleures espionnes du Service de renseignement de Sa Majesté, allie sensualité et férocité pour survivre à sa mission périlleuse. Envoyée en solo à Berlin pour livrer un dossier crucial dans une ville en pleine turbulence, elle s'allie à David Percival, chef de la station locale, déclenchant ainsi un dangereux jeu d'espionnage.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=237763.html"><em>ATOMIC BLOND</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_17" src="images/atomic_blond.jpg" class="atomic_blond" alt="affiche du film Atomic Blond">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
         "La Mémoire dans la peau" met en scène Matt Damon dans le rôle d'un homme retrouvé en mer, amnésique mais doté de compétences exceptionnelles en combat et en stratégie. Il découvre qu'il est traqué par des agents et se lance dans une quête pour découvrir son identité. Au fur et à mesure, il réalise qu'il est impliqué dans un programme secret du gouvernement et lutte pour échapper à son passé tout en dévoilant la vérité.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=29071.html"><em>LA MEMOIRE DANS LA PEAU</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_18" src="https://th.bing.com/th/id/OIP.e--QQ49n2rFj99A7321nagHaLH?rs=1&pid=ImgDetMain" class="atomic_blond" alt="affiche du film La Mémoire Dans La Peau">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
          "Nikita", une jeune femme désespérée, est recrutée de force par le gouvernement pour devenir une redoutable tueuse à gages. Sous la tutelle impitoyable de l'agence secrète, elle subit un entraînement intensif pour devenir une arme redoutable au service de l'État. Toutefois, lorsque ses sentiments pour un homme innocent ébranlent sa loyauté, Nikita doit choisir entre son devoir et sa liberté. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=8183.html"><em>NIKITA</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_19" src="images/NIKITA.jpg" class="Nikita" alt="affiche du film Nikita">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
          Dans "Skyfall", James Bond, l'agent secret emblématique, est confronté à un ennemi qui menace le MI6 et son propre passé. Alors qu'il affronte un cyberterroriste impitoyable, Bond doit faire face à ses propres démons et à une trahison au sein de l'agence. Dans une course contre la montre pour sauver le MI6, Bond est poussé au-delà de ses limites, mettant à l'épreuve sa loyauté envers M et son engagement envers sa mission. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=145646.html"><em>SKYFALL</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_20" src="images/james_bond.jpg" class="Nikita" alt="affiche du film James Bond: Skyfall">
      </div>
    </div>

   </div>
     
   <div class="overlay"></div>
     
</body>
</html>""" 
print(html)
     
     
     
     
     