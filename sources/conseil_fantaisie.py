# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 12:55:03 2024

@author: herve
"""


import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> MovieAdvice Fantaisie/fantastique </title>
    <link rel="stylesheet" href="Style/style_fantaisie.css">
</head>
<body>
     
   <div class="content">
    <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div><br><br><br><br>
     
    <div class="container">
      <div class="text-container">
        <p class="resume3">
          "Mon voisin Totoro" suit l'histoire de deux jeunes sœurs, Satsuki et Mei, qui découvrent un monde de magie et de merveilles après leur installation dans une nouvelle maison à la campagne. Elles rencontrent Totoro, une créature forestière géante et bienveillante, ainsi que d'autres esprits de la nature, qui deviennent leurs amis. Le film célèbre l'émerveillement de l'enfance et la connexion profonde entre les humains et la nature. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=14790.html"><em>MON VOISIN TOTORO</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_1" src="https://www.ecranlarge.com/media/cache/1600x1200/uploads/image/001/123/6hgeawckh2kzigsdhshnjf56itx-365.jpg" style="position: relative; width: 100%; height: auto; right: 75%; top: 40%;" alt="affiche du film Mon Voisin Totoro">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
      
     <div class="container">
       <div class="text-container">
         <p class="resume3">
           Dans "Le Château Ambulant", Sophie, une jeune femme, est transformée en vieille dame par une sorcière jalouse. Elle trouve refuge dans un mystérieux château ambulant appartenant au puissant sorcier Hurle. Sophie découvre que le château est habité par une multitude de créatures magiques et qu'il recèle de secrets étonnants. Elle se lie d'amitié avec Hurle et d'autres habitants du château, et ensemble, ils affrontent des défis magiques et des forces maléfiques.  
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=50409.html"><em>Le Château Ambulant</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_2" src="https://th.bing.com/th/id/OIP.zMDNOad4CgMBNmgGBDxUjQHaJ3?rs=1&pid=ImgDetMain" style="position: relative; width: 100%; height: auto; right: 75%; top: 40%;" alt="affiche du film Le château ambulant">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
          "Le Prince d'Égypte" est un film d'animation captivant qui retrace l'histoire de Moïse, de sa naissance à sa libération du joug égyptien. À travers des voyages dans le temps, le film explore des thèmes de liberté, de destinée et de rédemption. Avec des personnages mythiques et des événements spectaculaires, il offre une immersion dans l'ancienne Égypte et dans des récits bibliques.  
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27657.html"><em>Le Prince d'Égypte</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_3" src="https://media.senscritique.com/media/000007118725/source_big/Le_Prince_d_Egypte.jpg" style="position: relative; width: 80%; height: auto; right: 50%; top: 40%;" alt="affiche du film Le prince d'Egypte">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume">
         Dans "Shrek", un ogre solitaire nommé Shrek, accompagné de son fidèle compagnon l'Âne, se lance dans une quête pour sauver la princesse Fiona, gardée prisonnière dans un château enchanté. Sur leur chemin, ils rencontrent des personnages de contes de fées revisités de manière comique, y compris un méchant redoutable, Lord Farquaad. Entre aventures hilarantes et rebondissements inattendus, Shrek découvre le vrai sens de l'amitié et de l'amour.
         </p>
       </div>
       <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27415.html"><em>SHREK</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_4" src="https://th.bing.com/th/id/R.faa103064486ce40356831e365c392c9?rik=GoaCXoP7iyy%2b1Q&riu=http%3a%2f%2fwww.moviegeek.eu%2fwp-content%2fuploads%2f2015%2f01%2fshrek-forever-after-official-poster.jpeg&ehk=7z%2fh4FrR5U4U%2bUH7wD1iX%2bxdckwPnwe5BIIH3l50dvI%3d&risl=&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 40%; top: 40%;" alt="affiche du film Shrek">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
        Dans "Le Hobbit", Bilbo Bessac, un hobbit paisible, est entraîné dans une quête extraordinaire par le magicien Gandalf et un groupe de nains dirigé par Thorin Écu-de-Chêne. Leur objectif est de récupérer le royaume perdu des nains d'Erebor, qui est gardé par le redoutable dragon Smaug. Au cours de leur voyage, Bilbo fait face à de nombreux dangers, rencontre des créatures fantastiques comme les trolls, les elfes et les gobelins, et trouve un anneau magique qui changera son destin. 
         </p>
       </div>
       <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=119089.html"><em>LE HOBBIT</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_5" src="https://th.bing.com/th/id/OIP.j69bSsx0m-ISRlnIgeE1tQHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 80%; height: auto; right: 40%; top: 40%;" alt="affiche du film le hobbit">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
        Dans "Harry Potter à l'école des sorciers", un jeune garçon nommé Harry Potter découvre qu'il est un sorcier lorsqu'il est invité à rejoindre l'école de sorcellerie de Poudlard. Là, il se lie d'amitié avec Ron Weasley et Hermione Granger, et ensemble, ils enquêtent sur un mystère entourant une pierre magique qui accorde l'immortalité. Harry doit affronter le mage noir Voldemort, qui cherche à s'emparer de la pierre pour retrouver sa forme physique. 
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=29276.html"><em>HARRY POTTER</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_6" src="https://fr.web.img2.acsta.net/pictures/18/07/02/17/25/3643090.jpg" style="position: relative; width: 90%; height: auto; right: 52%; top: 40%;" alt="affiche du film Harry Potter">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
        "Pirates des Caraïbes : La Malédiction du Black Pearl" est une aventure captivante qui suit le capitaine Jack Sparrow dans sa quête pour retrouver son navire volé, le Black Pearl. Avec l'aide du forgeron Will Turner et de la fille du gouverneur, Elizabeth Swann, ils affrontent le cruel capitaine Barbossa et son équipage maudit. Le film regorge de combats d'épées, d'humour et de magie, avec des scènes inoubliables sur des îles exotiques et à bord de navires impressionnants.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=46117.html"><em>PIRATES DES CARAIBES</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_7" src="https://th.bing.com/th/id/OIP.j-d4Qex2H8AGvnL0jaOgEgHaJ3?rs=1&pid=ImgDetMain" style="position: relative; width: 100%; height: auto; right: 69%; top: 40%;" alt="affiche du film Pirates Des Caraïbes">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
       Dans l'adaptation cinématographique de "Le Livre de la jungle", Mowgli, un jeune garçon élevé par des loups, entreprend un voyage dans la jungle indienne. Accompagné de Baloo l'ours et de Bagheera la panthère noire, il rencontre des créatures magiques comme le serpent Kaa et affronte le redoutable tigre Shere Khan. Son périple est marqué par des aventures captivantes où il apprend des leçons de courage, d'amitié et de persévérance.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=222692.html"><em>Le Livre De La Jungle</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_8" src="https://th.bing.com/th/id/OIP.SmstCFCSyudF511sQVIiTwHaKF?rs=1&pid=ImgDetMain" style="position: relative; width: 100%; height: auto; right: 69%; top: 40%;" alt="affiche du film Le Livre De La Jungle">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
      Dans "Le Royaume des Chats" de Studio Ghibli, Haru, une jeune fille, se retrouve transportée dans un royaume habité par des chats après avoir sauvé un chaton. Elle est accueillie par le mystérieux Baron Humbert von Gikkingen et se trouve mêlée à une série d'aventures pour sauver le royaume. Avec ses nouveaux amis, Haru découvre le pouvoir de la compassion et de la détermination dans un monde magique où les chats ont des personnalités étonnamment humaines.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=52143.html"><em>Le Royaume des chats</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_9" src="https://th.bing.com/th/id/R.0aad0e3ba08a71c2a31feeb153689bf9?rik=Yitli%2f85gk8MrQ&riu=http%3a%2f%2fwww.leslecturesdeliyah.com%2fwp-content%2fuploads%2f2012%2f02%2fLe-royaume-des-chats-Miyazaki-Les-lectures-de-Liyah.jpg&ehk=w4aNq9ezlaFVRylkkWHRYMkhvJEs%2bYHtM7eWgsu0Ajw%3d&risl=&pid=ImgRaw&r=0" style="position: relative; width: 100%; height: auto; right: 62%; top: 40%;" alt="affiche du film Le Royaume des chats">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
      "L'Étrange Noël de Monsieur Jack" est un dessin animé fantastique qui suit l'histoire de Jack Skellington, le roi de la ville d'Halloween, qui découvre par hasard la ville de Noël. Fasciné par cette nouvelle découverte, il décide de s'approprier la fête de Noël, mais ses bonnes intentions se transforment en chaos. Ce film offre une animation unique et visuellement saisissante, mêlant l'esthétique sombre d'Halloween à la magie enchanteresse de Noël.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27633.html"><em>L'Étrange Noël de Jack</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_10" src="https://th.bing.com/th/id/R.541bedc098bff97fcb7c1c36aa4f2c4b?rik=D2lwbhNGdW4YZQ&pid=ImgRaw&r=0" style="position: relative; width: 100%; height: auto; right: 69%; top: 40%;" alt="affiche du film L'Étrange Noël de Monsieur Jack">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
        "Anastasia" est un dessin animé fantastique qui se déroule dans un contexte historique, s'inspirant de la légende de la grande-duchesse Anastasia Nikolaïevna de Russie. L'histoire suit une jeune fille qui, après avoir perdu la mémoire à la suite d'un traumatisme, croit être la dernière survivante de la famille impériale russe. Elle se lance alors dans un voyage pour retrouver sa véritable identité, tout en étant poursuivie par des forces maléfiques.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=3316.html"><em>ANASTASIA</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_11" src="https://th.bing.com/th/id/R.724d9f315085e2daff45b9503fbbb294?rik=eS9ZY%2faKx8vMxQ&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 45%; top: 40%;" alt="affiche du film Anastasia">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
        "Dorothy et le Magicien d'Oz" est une adaptation animée du classique "Le Magicien d'Oz". Dorothy, accompagnée de ses amis, entreprend un voyage à travers le pays d'Oz pour rencontrer le Magicien. Ils rencontrent des créatures magiques et affrontent des défis pour atteindre leur but. Le film mêle aventure, magie et amitié dans une interprétation moderne du conte original.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/rechercher/?q=Dorothy+et+le+Magicien+d%27Oz"><em>Le Magicien d'Oz</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_12" src="https://th.bing.com/th/id/R.8757ce286e0536394d0ab603250bc51b?rik=Zn45vg8IcLhJQQ&riu=http%3a%2f%2ffr.web.img6.acsta.net%2fpictures%2f210%2f380%2f21038079_20130909112247312.jpg&ehk=dX%2bcbiHgpTurxAVDhOC4GywJsei4NeQfkxnFXT6TMDE%3d&risl=&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 45%; top: 40%;" alt="affiche du film Dorothy et le Magicien d'Oz">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
        "Le Labyrinthe de Pan" est un conte sombre et envoûtant qui suit l'histoire d'Ofelia, une jeune fille plongée dans un monde fantastique rempli de créatures magiques. Pour prouver son héritage royal, elle doit accomplir trois tâches dangereuses. Le film mélange habilement la réalité sombre de l'Espagne post-civile avec le monde imaginaire d'Ofelia.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=57689.html"><em>Le Labyrinthe de Pan</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_13" src="https://th.bing.com/th/id/R.b396249b66112563880cbe275353e5f4?rik=%2fm9%2fWGbN49yFWA&pid=ImgRaw&r=0" style="position: relative; width: 100%; height: auto; right: 60%; top: 40%;" alt="affiche du film Le Labyrinthe de Pan">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume2">
        "L'étrange histoire de Benjamin Button" est un film captivant qui suit l'histoire de Benjamin, un homme né vieillard qui rajeunit physiquement au fil du temps. Adapté de la nouvelle éponyme de F. Scott Fitzgerald, le film explore les thèmes de l'amour, du temps qui passe et de l'acceptation de soi. Cette histoire poignante nous emmène dans un voyage émotionnel à travers les décennies, offrant une perspective unique sur la vie et la mortalité.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=57060.html"><em>Benjamin Button</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_14" src="https://th.bing.com/th/id/R.f15c0ab0dc2425a0645466e7965a5c09?rik=r9anXyYpHmd7iw&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 46%; top: 40%;" alt="affiche du film L'étrange histoire de Benjamin Button">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume">
        Dans "X-Men", une équipe de mutants dotés de pouvoirs extraordinaires, dirigée par le professeur Charles Xavier, lutte pour protéger un monde qui les craint et les rejette. Le conflit s'intensifie lorsque Magnéto, un mutant rebelle, cherche à déclencher une guerre entre les humains et les mutants. Au milieu de ces tensions, le jeune Wolverine découvre ses propres capacités et s'engage dans un combat pour la justice et la coexistence pacifique. 
         </p>
       </div>
       <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=25518.html"><em>X MEN</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_15" src="https://th.bing.com/th/id/R.70a2f9351cdd6a0fc91ed5dffc14ef49?rik=Vc%2fgIvcAxwrIqA&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 46%; top: 40%;" alt="affiche du film X MEN">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     
     <div class="container">
       <div class="text-container">
         <p class="resume3">
        Dans "La Belle et la Bête", une jeune femme nommée Belle se retrouve captive dans un château enchanté, où elle rencontre une Bête monstrueuse, en réalité un prince transformé par un sortilège. Malgré leurs débuts houleux, une relation improbable se développe entre eux alors que Belle découvre la véritable bonté de la Bête derrière son apparence. Leur histoire est marquée par des défis, des confrontations avec un chasseur de prime jaloux, Gaston.
         </p>
       </div>
       <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=203397.html"><em>LA BELLE ET LA BÊTE</a></em></h2></div>
       <div class="image-container">
         <img id="ancre_16" src="https://th.bing.com/th/id/OIP.-ZT7blev-FTDbqNi1WOougHaKk?rs=1&pid=ImgDetMain" style="position: relative; width: 100%; height: auto; right: 68%; top: 40%;" alt="affiche du film LA BELLE ET LA BÊTE">
       </div>
     </div>
     
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
     <br><br><br<<br><br>
    
   </div>
   
   <div class="overlay"></div>
    
</body>
</html>""" 
print(html)