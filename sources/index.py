# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 09:29:49 2024

@author: hdubois
"""


import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> Genre Film </title>
    <link rel="stylesheet" href="Style/style_index.css" >
    <script>
      function redirectToGenrePage() {
          var genre = document.querySelector('input[name="genre"]:checked').value;
          var form = document.getElementById('genreForm');
          switch (genre) {
              case 'ACTION':
                  form.action = 'film_action.py';
                  break;
              case 'HORREUR':
                  form.action = 'film_horreur.py';
                  break;
              case 'FANTASY':
                  form.action = 'film_fantaisie.py';
                  break;  
              case 'SCI-FICTION':
                  form.action = 'film_sci-fiction.py';
                  break;
              case 'ROMANCE':
                  form.action = 'film_romantique.py';
                  break;                   
              default:
                  form.action = 'default_page.py'; // Page par défaut si aucun genre ne correspond
          }
          return true; // Permet au formulaire de se soumettre après avoir modifié l'action
      }
  </script>
</head>
<body>

     <div class="content">
      <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div>
      
      <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Bienvenue sur MovieAdvice</strong></p></div><br>
      
      <div class="largeur_texte">
       <p>Plongez dans l'univers infini du cinéma avec CinéConseil, votre guide personnel pour des expériences cinématographiques inoubliables. Que vous soyez un cinéphile passionné à la recherche de votre prochain chef-d'œuvre ou simplement à la recherche d'un divertissement agréable, notre plateforme est là pour vous aider à trouver le film parfait, sur mesure.</p>
      </div>
      <br>
      
      <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Explorez nos recommandations personnalisées</strong></p></div><br>
      
      <div class="largeur_texte">
       <p>Découvrez des films qui correspondent exactement à vos goûts et à vos humeurs du moment. Notre algorithme avancé analyse vos préférences cinématographiques et vos recherches précédentes pour vous offrir des suggestions sur mesure. Que vous soyez fan de suspense, d'action, de romance ou de comédie, nous avons le film idéal pour chaque occasion.</p>
      </div>
      <br>
      
      <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Choisissez votre genre préféré</strong></p></div><br>
      
      <div class="largeur_texte">
       <p>Commencez votre voyage cinématographique en choisissant parmi une large gamme de genres, des classiques intemporels aux derniers blockbusters. Que vous soyez amateur de science-fiction futuriste, de drames émouvants ou de thrillers haletants, notre sélection diversifiée saura satisfaire toutes vos envies.</p>
      </div>
      <br>
      
      <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Laissez-vous surprendre</strong></p></div><br>
      
      <div class="largeur_texte">
       <p>Parfois, les meilleures découvertes cinématographiques se cachent là où on s'y attend le moins. Laissez-vous surprendre par nos recommandations spéciales et explorez de nouveaux horizons cinématographiques. Qui sait, vous pourriez trouver votre nouveau film préféré parmi nos suggestions uniques et inattendues.</p>
      </div>
      <br>
      
      <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Prêt à commencer ?</strong></p></div><br>
      
      <div class="largeur_texte">
       <p>Plongez dès maintenant dans l'univers infini du cinéma avec CinéConseil. Choisissez votre genre préféré ci-dessous et laissez-nous vous guider vers votre prochaine aventure cinématographique. Prêt à vivre des émotions inoubliables ? Cliquez, regardez, et laissez-vous emporter.</p>
      </div>
      <br>
      
      <div class="largeur_texte">
       <p style="text-decoration: underline;"><strong>Pour commencer, commencez par choisir quel genre de film vous sohaitez regarder.</strong></p>
      </div>
      
       <form method="post" action="resultats.py" id="genreForm" onsubmit="return redirectToGenrePage()">
     
      <div id="scene">
      
       <input type="radio" name="genre" value="ACTION" id="ACTION">
       <label for="ACTION">Film d'action</label><br>
     
       <input type="radio" name="genre" value="HORREUR" id="HORREUR">
       <label for="HORREUR">Film d'horreur</label><br>
     
       <input type="radio" name="genre" value="FANTASY" id="FANTASY">
       <label for="FANTASY">Film fantaisiste et fantastique</label><br>
     
       <input type="radio" name="genre" value="SCI-FICTION" id="SCI-FICTION">
       <label for="SCI-FICTION">Film de sciences fiction</label><br>
     
       <input type="radio" name="genre" value="ROMANCE" id="ROMANCE">
       <label for="ROMANCE">Film romantique</label><br>
     
      
      </div><br>
     
       <div> 
        <button type="submit" value="suivant"> Suivant </button>
       </div>
      
      
      </form>
     </div>
      
     <div class="overlay"></div>
     
</body>
</html>"""
print(html)
            
     
     