

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title>Page FANTAISIE</title>
    <link rel="stylesheet"  href="Style/style_fantaisie.css">	
</head>
<body>

   <div class="content">
    <div class="largeur_texte"><h1>FILM DE FANTAISIE</h1></div>
     
    <div class="largeur_texte"><h3>Explorez des Mondes Magiques avec le Cinéma de Fantaisie !</h3></div>
    
    <div class="largeur_texte">
     <p> Les films de fantaisie nous emmènent dans des voyages extraordinaires au-delà des limites de l'imagination, où la magie, l'aventure et les mystères règnent en maître !</p>
    </div>
    
    <div class="largeur_texte">
     <p><strong>Plongez dans des univers enchanteurs</strong>, où les héros intrépides se lancent dans des quêtes épiques, affrontant des dangers inouïs et découvrant des trésors cachés.</p>
    </div>
    
    <div class="largeur_texte">
     <p><strong>Laissez-vous emporter par la magie du cinéma</strong>, où chaque image est une invitation à rêver et où chaque histoire est une source d'émerveillement et d'évasion.</p>
    </div>
     
    
    <form method="post" action="redirect_fantaisie.py">
     
    <div class="largeur_texte"><p style="text-decoration: underline;">De la <strong>fantaisie</strong> ou du <strong>fantastique</strong>?</p></div>
      
    <div id="scene">
     
     <input type="radio" name="fantaisie1" value="fantaisie" id="fantaisie">
     <label for="fantaisie">De la fantaisie</label><br>
     <input type="radio" name="fantaisie1" value="fantastique" id="fantastique">
     <label for="fantastique">Du fantastique</label>
      
    </div><br>
    
    <div class="largeur_texte"><p style="text-decoration: underline;">Qu'est-ce qui vous plairez?</p></div>
     
    <div id="scene">
    
     <input type="radio" name="fantaisie2" value="concept1" id="concept1">
     <label for="concept1">Les créatures mystiques, légendaires, imaginaires</label><br>
     <input type="radio" name="fantaisie2" value="concept2" id="concept2">
     <label for="concept2"> Regardez les films adaptés de vos livre préférés, ou découvrez-en</label><br>
     <input type="radio" name="fantaisie2" value="concept3" id="concept3">
     <label for="concept3">Contexte historique</label><br>
     <input type="radio" name="fantaisie2" value="concept4" id="concept4">
     <label for="concept4"> Réinterprétation de contes, classiques...</label><br>
     
    </div><br>
    
    <div class="largeur_texte"><p style="text-decoration: underline;">Sous quel format voulez-vous regarder votre film?</p></div>
     
    <div id="scene">
    
     <input type="radio" name="fantaisie3" value="animation" id="animation">
     <label for="animation">Dessin animé</label><br>
     <input type="radio" name="fantaisie3" value="joue" id="joue">
     <label for="joue">Film joué</label>
     
    </div><br>
     
    <div><button type="submit" value="suivant">MovieAdvice</button></div>
    
    </form>
   </div>

   <div class="overlay"></div>
   
</body>
</html>""" 
print(html)

