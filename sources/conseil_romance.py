# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 18:19:23 2024

@author: herve
"""


import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> MovieAdvice Romance </title>
    <link rel="stylesheet" href="Style/style_romantique.css">
</head>
<body>
     
   <div class="content">
    <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div><br><br>
     
    <div class="container">
      <div class="text-container">
        <p class="resume">
          "The Duff" suit l'histoire de Bianca, une lycéenne considérée comme la "DUFF" de son groupe d'amis. Déterminée à changer cette étiquette, elle fait équipe avec son voisin populaire, Wesley, pour retrouver sa confiance en elle. Tout en naviguant dans les hauts et les bas de l'adolescence, Bianca et Wesley découvrent qu'ils ont plus en commun qu'ils ne le pensaient, et finissent par tomber amoureux.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=229260.html"><em>THE DUFF</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_1" src="images/duff.jpg" class="terminator" alt="affiche du film The Duff">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
          "The Kissing Booth" suit l'histoire d'Elle Evans, une lycéenne qui développe des sentiments pour le frère aîné de son meilleur ami, Noah Flynn. Leur amour est compliqué par les règles strictes de la famille de Noah, mais ils naviguent à travers les défis ensemble. Avec l'aide de son meilleur ami Lee, Elle jongle entre l'amitié et l'amour tout en essayant de garder leur relation secrète. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=252178.html"><em>THE KISSING BOOTH</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_2" src="images/booth.jpg" style="position: relative; width: 80%; height: auto; right: 55%; top: 40%;" alt="affiche du film The Kissing Booth">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
          Dans "Nos étoiles contraires", Hazel Grace Lancaster, une adolescente atteinte de cancer, rencontre Augustus Waters lors d'un groupe de soutien. Leur amitié évolue rapidement en une histoire d'amour sincère alors qu'ils partagent des moments intenses et significatifs malgré leurs défis de santé. Ensemble, ils entreprennent un voyage pour rencontrer leur auteur préféré, ce qui les conduit à des découvertes sur la vie, l'amour et la mort. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=218926.html"><em>ETOILES CONTRAIRES</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_3" src="https://pgw.udn.com.tw/gw/photo.php?u=https://uc.udn.com.tw/photo/2015/06/11/99/924287.jpg&x=0&y=0&sw=0&sh=0&sl=W&fw=750" style="position: relative; width: 80%; height: auto; right: 55%; top: 40%;" alt="affiche du film Nos Etoiles Contraires">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
          Clay Jensen, un lycéen qui découvre une série de cassettes enregistrées par son amie Hannah Baker, décédée par suicide. À travers les cassettes, Hannah explique les treize raisons qui l'ont poussée à mettre fin à sa vie. La série explore des thèmes sombres tels que le harcèlement, l'agression sexuelle et la dépression, et se termine par une conclusion émotionnelle et déchirante.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=19941.html"><em>13 REASONS WHY</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_4" src="images/13reasons.jpg" class="edge" alt="affiche du film 13 Reasons Why">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
         "The Notebook" est une histoire d'amour épique entre Noah et Allie, deux jeunes amoureux que la vie a séparés. Malgré les défis, leur amour persiste à travers les années. Le film explore les thèmes de la passion, du sacrifice et du destin. À la fin, Noah et Allie se retrouvent dans une touchante scène finale, où leur amour triomphe de toutes les épreuves. Cette conclusion heureuse laisse les spectateurs avec une lueur d'espoir et une croyance en l'amour éternel.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=47422.html"><em>THE NOTEBOOK</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_5" src="images/notebook.jpg" class="edge" alt="affiche du film The Notebook">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         "Outlander" suit l'histoire de Claire Randall, une infirmière de la Seconde Guerre mondiale qui se retrouve transportée dans l'Écosse du 18ème siècle, où elle tombe amoureuse de Jamie Fraser, un guerrier écossais. Leur amour est mis à l'épreuve par les conflits politiques et les dangers de l'époque, mais persiste malgré tout. À travers les saisons, ils affrontent ensemble des défis qui renforcent leur lien.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=11266.html"><em>OUTLANDER</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_6" src="images/outlander.jpg" class="edge" alt="affiche du film Outlander">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         "Titanic" narre l'histoire de Jack et Rose, deux jeunes amoureux issus de classes sociales opposées embarqués à bord du paquebot Titanic. Leur idylle naissante est brutalement interrompue par le tragique naufrage du navire après une collision avec un iceberg. Malgré les efforts désespérés de Jack pour sauver Rose, il trouve la mort dans les eaux glacées de l'Atlantique. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=5818.html"><em>TITANIC</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_7" src="images/titanic.jpg" class="terminator" alt="affiche du film Titanic">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
        Dans "A Million Little Things", un groupe d'amis de Boston est lié par le suicide tragique d'un des leurs. Alors qu'ils tentent de naviguer à travers les épreuves de la vie, des secrets et des tensions émergent, mettant à l'épreuve leurs liens. Malgré les moments de bonheur et de solidarité, la série prend une tournure sombre lorsque la mort frappe à nouveau, révélant les conséquences déchirantes de la détresse émotionnelle. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=22544.html"><em>MILLION LITTLE THINGS</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_8" src="images/a_million_little_things.webp" style="position: relative; width: 100%; height: auto; right: 75%; top: 40%;" alt="affiche du film A Milliion Little Things">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
        Dans "Carol", Therese, une jeune photographe, croise le chemin de Carol, une femme plus âgée en instance de divorce. Une relation intense et passionnée se développe entre elles malgré les défis sociaux et personnels. Alors qu'elles naviguent à travers les obstacles de leur amour interdit, elles trouvent un refuge l'une dans l'autre. Finalement, leur lien se renforce et elles choisissent de suivre leur cœur
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=22544.html"><em>CAROL</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_9" src="https://teaser-trailer.com/wp-content/uploads/Carol-Poster.jpg" class="terminator" alt="Carol">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       "The L Word" suit les vies et les amours d'un groupe de femmes lesbiennes, bisexuelles et transgenres à Los Angeles. À travers les hauts et les bas de la vie, les personnages naviguent à travers les complexités de leurs relations et de leurs identités. Malgré les défis auxquels ils sont confrontés, plusieurs arcs narratifs se terminent par des fins heureuses, offrant ainsi une représentation positive des relations LGBT et de l'épanouissement personnel.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=198.html"><em>THE L WORLD</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_10" src="https://www.themoviedb.org/t/p/original/sB2vHcCLHJujDcH7vkaSjKyuikP.jpg" class="terminator" alt="Affiche du film The L World">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
       "Blue Is the Warmest Color" suit l'histoire d'amour intense entre Adèle et Emma, deux femmes dont la relation est confrontée à des défis. Malgré leur passion dévorante, leur écart de maturité et leurs différences d'aspirations les éloignent progressivement. La séparation finale est déchirante, laissant Adèle seule et en proie à un chagrin profond. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.imdb.com/title/tt2278871/"><em>Blue Is the Warmest Color</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_11" src="https://th.bing.com/th/id/R.c59a0f21de047f230fc0e59f37d5b454?rik=WYhdoAsGDVnBtw&riu=http%3a%2f%2fwww.filmofilia.com%2fwp-content%2fuploads%2f2013%2f05%2fBlue-Is-the-Warmest-Colour-Poster.jpg&ehk=6m1g5hEdX%2bl4C41Vm7dTGuBrRD2VizSwC0HhxGdLhlA%3d&risl=&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 59%; top: 40%;" alt="Affiche du film Blue Is the Warmest Color">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
       "Julie and the Phantoms" suit l'histoire de Julie, une adolescente en deuil, qui forme un groupe de musique avec trois fantômes de musiciens décédés. Au fil de la série, Julie développe des sentiments pour Luke, l'un des membres du groupe, malgré les défis de leur amour interdimensionnel. Leur relation est brutalement interrompue lorsque Luke doit partir vers l'au-delà, laissant Julie seule et déchirée. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=26989.html"><em>Julie and the Phantoms</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_12" src="https://th.bing.com/th/id/R.18c97bba8e2af42b13872323bdd93526?rik=sy%2fCjw4apvdh%2fA&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 55%; top: 40%;" alt="affiche film Julie and the Phantoms">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       Dans "Love, Simon", Simon, un adolescent gay, entame une correspondance en ligne avec un autre garçon de son école. Alors qu'il cherche à découvrir l'identité de son mystérieux correspondant, Simon navigue à travers les hauts et les bas de l'adolescence et de l'amour. Malgré les défis et les peurs, Simon trouve finalement le courage de révéler sa propre vérité au grand jour. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=250073.html"><em>LOVE, SIMON</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_13" src="https://th.bing.com/th/id/OIP.HvsWaTE7QFBKxviSumKkxQHaJ4?rs=1&pid=ImgDetMain" class="edge" alt="affiche film Love, Simon">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
       "Queer as Folk" présente les vies et les relations d'un groupe d'hommes gays à Pittsburgh, explorant les hauts et les bas de l'amour et de l'identité queer. À travers les épreuves et les triomphes, la série offre des histoires émouvantes et authentiques de croissance personnelle et d'amour. Avec une représentation franche et nuancée, elle célèbre la force et la diversité de la communauté LGBT.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=9.html"><em>QUEER AS FOLK</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_14" src="https://th.bing.com/th/id/OIP.c27krh4ihbA2Ng6fiFYxKAHaLH?rs=1&pid=ImgDetMain" class="edge" alt="affiche film Queer As Folk">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
       "Brokeback Mountain" narre l'histoire d'amour clandestine entre deux cow-boys, Ennis Del Mar et Jack Twist, dans les vastes étendues du Wyoming. Malgré leur connexion profonde, leur relation est assaillie par les normes sociales conservatrices et les pressions extérieures. La tragédie frappe lorsque leur amour interdit est découvert, conduisant à des conséquences dévastatrices pour les deux hommes.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=54989.html"><em>Brokeback Mountain</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_15" src="https://th.bing.com/th/id/R.c5e3f1a38e98bf2bb492852684be5719?rik=oGj9R58NCByScg&pid=ImgRaw&r=0" style="position: relative; width: 75%; height: auto; right: 50%; top: 40%;" alt="affiche film Brokeback Mountain">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       "Looking" est une série qui suit la vie de trois amis gays à San Francisco, explorant les complexités de l'amour et de l'identité. Malgré leurs espoirs de bonheur, la série se termine de manière tragique, révélant les défis et les luttes des hommes LGBT pour la réalisation personnelle. Les personnages sont confrontés à des épreuves déchirantes, mettant en lumière les sacrifices nécessaires pour vivre authentiquement dans une société parfois hostile. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie-16928/critiques/"><em>LOOKING</a></em></h2></div>
      <div class="image-container">
        <img id="ancre_16" src="https://th.bing.com/th/id/OIP.CR9PsHnok8hOdLl6bomu4QHaJo?rs=1&pid=ImgDetMain" style="position: relative; width: 75%; height: auto; right: 43%; top: 40%;" alt="affiche film Looking">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
   </div>
     
   <div class="overlay"></div>
     
</body>
</html>""" 
print(html)