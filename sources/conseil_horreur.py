# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 15:57:40 2024

@author: herve
"""


import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> MovieAdvice Horreur </title>
    <link rel="stylesheet" href="Style/style_horreur.css" >
</head>
<body>
     
   <div class="content">
    <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div><br><br><br>
     
    <div class="container">
      <div class="text-container">
        <p class="resume">
          Dans "Hereditary", une famille est confrontée à des événements troublants et inexplicables après le décès de leur matriarche. Alors que des secrets de famille sombres commencent à émerger, ils se retrouvent plongés dans un cauchemar terrifiant, hantés par des forces surnaturelles. Le film explore les thèmes de la perte, du deuil et de la folie, tandis que l'atmosphère oppressante et les scènes visuellement dérangeantes captivent les spectateurs. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=261860.html"><em>HEREDITARY</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_1" src="images/hereditary.jpg" style="position: relative; width: 90%; height: auto; right: 60%; top: 40%;" alt="affiche du film Hereditary">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         "Shutter Island" est un thriller psychologique captivant qui suit deux enquêteurs envoyés sur une île psychiatrique isolée pour résoudre la disparition mystérieuse d'une patiente. Alors qu'ils explorent les recoins sombres de l'île, des événements troublants commencent à se produire, remettant en question la santé mentale des enquêteurs et la réalité même de leur environnement. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=132039.html"><em>SHUTTER ISLAND</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_2" src="https://th.bing.com/th/id/R.9726172ece8b49a37fd03c2ce7b29d9d?rik=TCy%2f9TiU6KrLEg&riu=http%3a%2f%2fcdn.collider.com%2fwp-content%2fuploads%2fshutter-island-movie-poster.jpg&ehk=AiJZYd7WkY5Zohc%2bEkXQ1pE%2fGHKIO7wkh3MOKCjQ8WE%3d&risl=&pid=ImgRaw&r=0" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film Shutter Island">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         "The Sixth Sense" est un film d'horreur psychologique qui suit un jeune garçon, Cole, qui prétend voir des fantômes et un psychologue, le Dr Malcolm Crowe, qui tente de l'aider. Alors que Crowe tente de comprendre la vérité derrière les visions de Cole, le film explore des thèmes de peur, de deuil et de rédemption. Les événements surnaturels se déroulent de manière subtile, créant une tension palpable sans recourir à des scènes trop effrayantes. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=22092.html"><em>THE SIXTH SENSE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_3" src="https://th.bing.com/th/id/OIP.qj6farUghZKeKCxiTt_qnQHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film The Sixth Sense">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         "Insidious" est un film d'horreur surnaturel qui suit une famille confrontée à des événements inquiétants après que leur fils soit tombé dans un coma mystérieux. Avec des apparitions fantomatiques et des séquences effrayantes, le film offre une expérience terrifiante. Les jumpscares sont habilement utilisés pour maintenir une tension constante. L'atmosphère sombre et oppressante captive les spectateurs, les laissant sur le bord de leur siège.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=182603.html"><em>INSIDIOUS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_4" src="https://th.bing.com/th/id/R.1d943f12e4911e650ded0909f5315682?rik=0JbCbjuLS6WG9A&pid=ImgRaw&r=0" style="position: relative; width: 80%; height: auto; right: 48%; top: 40%;" alt="affiche du film Insidious">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         Dans "Evil Dead", un groupe d'amis se retrouve piégé dans une cabane isolée où ils libèrent involontairement des forces démoniaques en lisant un ancien livre. Au fur et à mesure que le mal se propage, ils sont confrontés à des horreurs indicibles et à des possessions maléfiques. Les scènes sanglantes abondent alors que les protagonistes luttent pour leur survie contre des forces surnaturelles terrifiantes. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=188.html"><em>EVIL DEAD</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_5" src="https://th.bing.com/th/id/OIP.8nuImeZbWFRLg_MQ-2fUcgHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 80%; height: auto; right: 48%; top: 40%;" alt="affiche du film EVIL DEAD">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
        "L'Orphelinat" est un film d'horreur captivant où une mère, Laura, revient avec sa famille pour restaurer un ancien orphelinat où elle a passé son enfance. Son fils commence à communiquer avec des amis imaginaires, mais Laura réalise rapidement que ces amis pourraient être des fantômes d'enfants décédés autrefois à l'orphelinat. Les événements surnaturels s'intensifient alors que Laura cherche désespérément à découvrir la vérité sur ce qui s'est passé dans cet endroit lugubre. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=128364.html"><em>L'ORPHELINAT</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_6" src="https://th.bing.com/th/id/OIP.syzCT5ocDZ3n3kW_rXzhVAHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 80%; height: auto; right: 48%; top: 40%;" alt="affiche du film L'orphelinat">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
        Dans "Cloverfield", un groupe d'amis new-yorkais filme leur tentative désespérée de survivre à une attaque monstrueuse dévastant la ville. Leur enregistrement amateur capture l'horreur et le chaos alors qu'ils luttent pour échapper au monstre titanesque et aux créatures mystérieuses qui l'accompagnent. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=129846.html"><em>CLOVERFIELD</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_7" src="https://th.bing.com/th/id/OIP.PTVOGAoId7q2RjAkYsxRfQHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 80%; height: auto; right: 48%; top: 40%;" alt="affiche du film Cloverfield">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       Dans "Tremors", une petite ville isolée est subitement menacée par d'énormes créatures souterraines appelées Graboïdes, qui chassent leurs proies en détectant les vibrations du sol. Lorsque les habitants réalisent le danger qui les guette, ils doivent unir leurs forces pour survivre contre ces monstres. Le film mélange habilement des éléments d'horreur, d'humour et d'action, créant ainsi une atmosphère palpitante et divertissante.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=5674.html"><em>TREMORS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_8" src="https://th.bing.com/th/id/OIP.2VA7nRWhylikrvny5juzgAAAAA?rs=1&pid=ImgDetMain" style="position: relative; width: 60%; height: auto; right: 30%; top: 40%;" alt="affiche du film Tremors">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       "The Ritual" suit un groupe d'amis en randonnée en Scandinavie pour honorer un camarade décédé. Lorsqu'ils s'aventurent dans la forêt, ils se retrouvent confrontés à des forces surnaturelles terrifiantes qui les traquent. Le film mêle habilement le paysage magnifique de la nature sauvage avec une atmosphère sombre et oppressante. Les membres du groupe doivent affronter leurs propres démons intérieurs tout en luttant contre les dangers surnaturels qui les entourent.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=256519.html"><em>THE RITUALS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_9" src="https://th.bing.com/th/id/OIP.C8Aw3jKxBH0mX0UWrFkdGwHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 60%; height: auto; right: 30%; top: 40%;" alt="affiche du film The Rituals">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       "Sinister" suit un écrivain de crime qui, après avoir emménagé dans une maison où des meurtres ont eu lieu, découvre d'anciennes vidéos macabres. Au fur et à mesure qu'il enquête sur ces enregistrements, il libère une force démoniaque qui menace sa famille. Le film est salué pour son ambiance terrifiante et ses scènes de suspense tendues, où chaque nouvelle découverte plonge le protagoniste et le spectateur dans un abîme de terreur. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=200962.html"><em>SINISTER</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_10" src="https://th.bing.com/th/id/R.203881372a023883e5618d473912b550?rik=ibElV3wJpXMulQ&pid=ImgRaw&r=0" style="position: relative; width: 60%; height: auto; right: 30%; top: 40%;" alt="affiche du film Sinister">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
       "L'Exorcisme d'Emily Rose" suit le procès d'un prêtre pour la mort d'Emily Rose lors d'un exorcisme. Entre flashbacks troublants et séquences de tribunal tendues, le film explore les événements surnaturels entourant la jeune femme. Les scènes d'exorcisme intenses alternent avec des moments de suspense psychologique, créant une atmosphère terrifiante.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=59130.html"><em>L'Exorcisme d'Emily Rose</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_12" src="https://th.bing.com/th/id/OIP.LKq2AVeX0w8AnFMIDo4KvAHaLH?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 60%; top: 40%;" alt="affiche du film L'Exorcisme d'Emily Rose">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
       "Dans le noir" suit une famille aux prises avec une entité maléfique qui se cache dans l'obscurité. Alors que Rebecca et son frère Martin découvrent le sombre passé de leur mère lié à cette force surnaturelle, ils sont confrontés à des événements terrifiants mettant leur vie en péril. Le film offre des frissons constants grâce à une atmosphère oppressante et des apparitions soudaines. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=238670.html"><em>DANS LE NOIR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_13" src="https://th.bing.com/th/id/OIP.qaLRBiaKpK-S7LFXCjgs7wHaJ4?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film L'Exorcisme d'Emily Rose">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
       Dans "La Nonne", une jeune religieuse et un prêtre sont envoyés en Roumanie pour enquêter sur le suicide d'une nonne. Ils découvrent un ancien secret sombre qui les met en danger alors qu'ils font face à une entité maléfique sous la forme d'une nonne démoniaque. Les deux protagonistes doivent affronter leurs propres peurs et doutes tout en luttant pour survivre contre des forces surnaturelles terrifiantes.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=248258.html"><em>LA NONNE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_14" src="https://eijwvqaycbm.exactdn.com/wp-content/uploads/2018/06/The-Nun-movie-poster-1200x1778.jpg" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film La Nonne">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
       "L'Exorciste" raconte l'histoire troublante d'une jeune fille possédée par une force démoniaque, conduisant sa mère à demander l'aide de prêtres pour l'exorciser. Les prêtres font face à des phénomènes surnaturels terrifiants pendant le rituel, mettant en lumière une lutte entre le bien et le mal. Ce classique de l'horreur est célèbre pour ses scènes intenses et ses effets spéciaux révolutionnaires. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27765.html"><em>L'EXORCISTE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_15" src="https://www.ecranlarge.com/uploads/image/001/215/4yxrumox2rxkh5oxupeeigkqoly-831.jpg" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film L'Exorciste">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
      Dans "The Descent", un groupe de femmes part en expédition dans des grottes peu explorées, mais elles se retrouvent piégées par des créatures souterraines terrifiantes. Les tensions montent au sein du groupe alors qu'elles luttent pour leur survie dans l'obscurité oppressante des cavernes. Entre les attaques des monstres et les conflits internes, elles doivent affronter leurs peurs les plus profondes pour s'échapper vivantes. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=47101.html"><em>THE DESCENT</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_16" src="https://th.bing.com/th/id/OIP.36_LBocpPaYOpidE6FFuPgDNEv?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 50%; top: 40%;" alt="affiche du film The descent">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
      "La Forme de l'eau" est une histoire fascinante qui se déroule dans les années 1960, centrée sur Elisa, une femme muette travaillant dans un laboratoire gouvernemental hautement sécurisé. Sa vie bascule lorsqu'elle découvre et développe une connexion profonde avec une mystérieuse créature aquatique captive. Alors que leur lien se renforce, Elisa et ses amis conspirent pour libérer la créature avant qu'elle ne devienne l'objet d'expériences inhumaines.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=246009.html"><em>LA FORME DE L'EAU</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_17" src="https://th.bing.com/th/id/OIP.VdVtJdRt4jVNqtEixqO_KQHaKD?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 51%; top: 40%;" alt="affiche du film La forme de l'eau">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
      Dans "Conjuring", basé sur des événements réels, les Warren, un couple de chasseurs de démons, enquêtent sur une maison hantée par une présence maléfique qui terrorise une famille. Alors qu'ils découvrent les sombres secrets de la demeure, les Warren doivent affronter des forces démoniaques terrifiantes pour protéger la famille et mettre fin à la malédiction. Les événements surnaturels se multiplient, mettant à rude épreuve les compétences et la foi des Warren face à un ennemi déterminé à semer la destruction.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=203607.html"><em>CONJURING</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_18" src="https://th.bing.com/th/id/OIP.3o-QVdMC2ij76AqDmcF2cgHaHa?rs=1&pid=ImgDetMain" style="position: relative; width: 90%; height: auto; right: 51%; top: 40%;" alt="affiche du film Conjuring">
      </div>
    </div>
    
   </div>
   
   <div class="overlay"></div>
     
</body>
</html>""" 
print(html)