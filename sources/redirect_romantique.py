
import cgi
import urllib.parse

print("Content-type: text/html; charset=UTF-8")
print()

# Récupération des données postées par le formulaire
form = cgi.FieldStorage()

# Extraction des choix de l'utilisateur
romance1 = form.getvalue('romance1')
romance2 = form.getvalue('romance2')
romance3 = form.getvalue('romance3')

# Déterminer l'ancre en fonction des choix de l'utilisateur
try:
    if romance1 == 'trope1' and romance2 == 'film' and romance3 == 'happy': # THE DUFF
        ancre = 'ancre_1'
    elif romance1 == 'trope1' and romance2 == 'film2' and romance3 == 'happy': # THE KISSING BOOTH
        ancre = 'ancre_2'
    elif romance1 == 'trope1' and romance2 == 'film' and romance3 == 'sad': # NOS ETOILES CONTRAIRES
        ancre = 'ancre_3'
    elif romance1 == 'trope1' and romance2 == 'film2' and romance3 == 'sad': # 13 REASONS WHY
        ancre = 'ancre_4'
    elif romance1 == 'trope2' and romance2 == 'film' and romance3 == 'happy': # THE NOTEBOOK
        ancre = 'ancre_5'
    elif  romance1 == 'trope2' and romance2 == 'film2' and romance3 == 'happy': # OUTLANDER
        ancre = 'ancre_6'
    elif romance1 == 'trope2' and romance2 == 'film' and romance3 == 'sad': # TITANIC
        ancre = 'ancre_7'
    elif romance1 == 'trope2' and romance2 == 'film2' and romance3 == 'sad': # A MILLIION LITTLE THINGS 
        ancre = 'ancre_8'
    elif romance1 == 'trope3' and romance2 == 'film' and romance3 == 'happy': # CAROL
        ancre = 'ancre_9'
    elif romance1 == 'trope3' and romance2 == 'film2' and romance3 == 'happy': # THE L WORLD
        ancre = 'ancre_10'
    elif romance1 == 'trope3' and romance2 == 'film' and romance3 == 'sad': # Blue Is the Warmest Color
        ancre = 'ancre_11'
    elif romance1 == 'trope3' and romance2 == 'film2' and romance3 == 'sad': # Julie and the Phantoms
        ancre = 'ancre_8'
    elif romance1 == 'trope4' and romance2 == 'film' and romance3 == 'happy': # LOVE SIMON
        ancre = 'ancre_13'
    elif romance1 == 'trope4' and romance2 == 'film2' and romance3 == 'happy': # QUEER AS FOLK
        ancre = 'ancre_14'
    elif romance1 == 'trope4' and romance2 == 'film' and romance3 == 'sad': # Brokeback Mountain
        ancre = 'ancre_15'
    elif romance1 == 'trope4' and romance2 == 'film2' and romance3 == 'sad': # LOOKING
        ancre = 'ancre_16'
    else:
        raise Exception
except:
    if romance1 is None or romance2 is None or romance3 is None:
        print("<h1>Revenez en arrière, il vous faut répondre à toutes les questions.</h1>")
else:
    # Construire l'URL de redirection vers la troisième page avec les paramètres du formulaire et l'ancre appropriée
    redirect_url = f"conseil_romance.py?romance1={romance1}&romance2={romance2}&romance3={romance3}#{ancre}"


#Rediriger l'utilisateur
print(f"<script>window.location.href = '{redirect_url}';</script>")

