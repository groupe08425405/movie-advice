# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 09:17:26 2024

@author: hdubois
"""

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title>ROMANCE</title>
    <link rel="stylesheet"  href="Style/style_romantique.css">	
</head>
<body>

   <div class="content">
    <div class="largeur_texte"><h1>FILM ROMANTIQUE</h1></div>
     
    <div class="largeur_texte"><h3 style="text-decoration: underline;"><strong>Découvrez la Magie du Cinéma Romantique !</strong></h3></div><br>
    
    <div class="largeur_texte">
     <p>Explorez un monde de possibilités où chaque rencontre est une aventure, chaque baiser est électrisant et chaque déclaration est un moment de vérité. Les films d'amour captivent nos cœurs et nous transportent dans des univers où l'amour triomphe toujours.</p>
    </div>
    
     
     <form method="post" action="redirect_romantique.py" id="actionForm1">
     
    <div class="largeur_texte"><p style="text-decoration: underline;">Quelle trope?</p></div>
     
     <div id="scene">
     
     <input type="radio" name="romance1" value="trope1" id="trope1">
     <label for="trope1">Romance pour adolescent</label><br>
     <input type="radio" name="romance1" value="trope2" id="trope2">
     <label for="trope2">Romance pour adulte</label><br>
     <input type="radio" name="romance1" value="trope3" id="trope3">
     <label for="trope3">Romance LGBT girl</label><br>
     <input type="radio" name="romance1" value="trope4" id="trope4">
     <label for="trope4">Romance LGBT boy</label><br>
      
     </div><br>
    
     <div class="largeur_texte"><p style="text-decoration: underline;">Film ou Série?</p></div>
     
    <div id="scene">
    
     <input type="radio" name="romance2" value="film" id="film">
     <label for="film">Un film romantique</label><br>
     <input type="radio" name="romance2" value="film2" id="film2">
     <label for="film2">Une série romantique</label><br>
     
    </div><br>
     
    <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Quelle serait la fin de l'histoire?</strong></p></div>
    
    <div id="scene">
    
    <input type="radio" name="romance3" value="happy" id="happy">
    <label for="happy">Une Happy End</label><br>
    <input type="radio" name="romance3" value="sad" id="sad">
    <label for="sad">Une Sad End</label><br>
     
    </div><br>
       
    <div><button type="submit" value="suivant">MovieAdvice</button></div>
    
    </form>
   </div>

   <div class="overlay"></div>
   
</body>
</html>""" 
print(html)

