# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 08:23:13 2024

@author: hdubois
"""

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> Film Action </title>
    <link rel="stylesheet" href="Style/style_action.css" >
</head>
<body>

   <div class="content">
    <div class="largeur_texte"><h1>FILM D'ACTION</h1></div>
     
    <div class="largeur_texte"><p style="text-decoration: underline">Plongez dans l'univers explosif du film d'action !</p></div><br>
    
    <div class="largeur_texte">
     <p>L'action est au rendez-vous avec des scènes palpitantes, des combats épiques et des cascades à couper le souffle qui vous transporteront dans un tourbillon d'émotions fortes.</p>
     <p>Préparez-vous à être captivé par des courses-poursuites endiablées, des fusillades intenses et des confrontations musclées qui vous laisseront sans voix.</p>
     <p> Des effets spéciaux spectaculaires et une tension permanente vous tiendront en haleine du début à la fin, créant une expérience cinématographique inoubliable.</p>
     <p>Rejoignez les héros intrépides alors qu'ils affrontent des dangers sans précédent et défient l'impossible dans un spectacle d'action à couper le souffle !</p>
     <p>Préparez-vous à vivre une aventure intense et palpitante avec le film d'action, où chaque instant est une montée d'adrénaline et une explosion de sensations fortes !</p>
    </div>
     
    <div class="largeur_texte">
     <form method="post" action="redirect_action.py" id="actionForm1">
     <p>CHOISISSEZ UNE SCENE D'ACTION: <em> CHOISISSEZ EN UNE PREDOMINANTE, CEPENDANT D'AUTRES PEUVENT SE RETROUVER DANS UN MEME FILM.</em></p>
    </div>
     
    <div id="scene">
    
     <input type="radio" name="action1" value="scene1" id="scene1">
     <label for="scene1">Courses poursuites</label><br>
     <input type="radio" name="action1" value="scene2" id="scene2">
     <label for="scene2">Combats</label><br>
     <input type="radio" name="action1" value="scene3" id="scene3">
     <label for="scene4">Explosions/destruction</label><br>
     <input type="radio" name="action1" value="scene4" id="scene4">
     <label for="scene5">Cascades/acrobaties</label><br>
     <input type="radio" name="action1" value="scene5" id="scene5">
     <label for="scene6">Infiltration/espionnage</label><br>
     
    </div><br>
     
    <div class="largeur_texte"><p>DANS QUEL CONTEXTE SE PASSERAIT L'HISTOIRE?</p></div>
     
    <div id="scene">
    
     <input type="radio" name="action2" value="contexte1" id="contexte1">
     <label for="contexte1">Contexte plutôt réaliste</label><br>
     <input type="radio" name="action2" value="contexte2" id="contexte2">
     <label for="contexte2">Contexte comprenant des éléments de science-fiction voire fantastique</label>
     
    </div><br>
     
    <div class="largeur_texte"><p>QUEL SERAIT LE HEROS PRINCIPAL?</p></div>
     
    <div id="scene">
    
     <input type="radio" name="action3" value="GIRL" id="GIRL">
     <label for="GIRL">Personnage féminin</label><br>
     <input type="radio" name="action3" value="BOY" id="BOY">
     <label for="BOY">Personnage masculin</label>
     
    </div><br>
     
    <div><button type="submit" value="suivant">MovieAdvice </button></div>
    
    </form>    
   </div>
   
   <div class="overlay"></div>
       
</body>
</html>""" 
print(html)
  
   
    
    
    
    