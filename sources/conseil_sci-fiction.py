# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 17:14:14 2024

@author: herve
"""

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title> MovieAdvice Science-fiction </title>
    <link rel="stylesheet" href="Style/style_sci-fiction.css" >
</head>
<body>
     
   <div class="content">
    <div class="largeur_texte"><h1>MOVIE ADVICE</h1></div>
    
    <br><br><br>
     
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          "Star Wars" est une saga épique de science-fiction se déroulant dans une galaxie lointaine, très lointaine. L'histoire suit les aventures de Luke Skywalker, un jeune fermier qui découvre qu'il est lié à la Force, une énergie mystique, et se joint à l'Alliance Rebelle pour lutter contre l'Empire tyrannique. Avec l'aide de ses alliés, dont la princesse Leia, le contrebandier Han Solo et les droïdes R2-D2 et C-3PO, Luke se lance dans une quête pour vaincre l'Empire et son redoutable seigneur Sith, Dark Vador.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=20754.html"><em>STARWARS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_1" src="images/starwars.jpg" class="baby_driver" alt="affiche du film STARWARS">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Outland", le marshal fédéral William T. O'Niel est envoyé sur une colonie minière isolée en Io, une lune de Jupiter, pour enquêter sur une série de morts mystérieuses. Alors qu'il découvre un complot de corruption et de trafic de drogue impliquant des médicaments destinés à augmenter la productivité des mineurs, O'Niel doit faire face à des menaces mortelles. Aidé par ses collègues et des robots utilisés pour la maintenance de la station, il tente de démanteler le réseau criminel avant qu'il ne soit trop tard.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=998.html"><em>OUTLAND</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_2" src="images/outland.jpg" class="mad_max" alt="affiche du film OutLand">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
         "Les Gardiens de la Galaxie" suit Peter Quill, alias Star-Lord, et son équipe improbable de super-héros dans une aventure intergalactique. Ensemble, ils s'unissent pour protéger une relique convoitée, tout en affrontant des ennemis redoutables, dont Ronan l'Accusateur. Avec une dose d'humour, d'action et d'émotion, le film explore les relations entre les membres de l'équipe, y compris Gamora, Drax, Rocket et Groot, un arbre humanoïde.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=196604.html"><em>LES GARDIENS DE LA GALAXIE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_3" src="images/GardiensGalaxie.jpg" class="gardiens" alt="affiche du film Les Gardiens De La Galaxie">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         Dans "Valerian et la Cité des mille planètes", les agents spéciaux Valérian et Laureline sont chargés de protéger Alpha, une station spatiale cosmopolite abritant des milliers d'espèces extraterrestres. Lorsqu'une force obscure menace l'équilibre de l'univers, ils se lancent dans une mission périlleuse pour découvrir la vérité derrière cette menace. Armés de leur ingéniosité et de leurs compétences, ils affrontent des dangers sans précédent dans un voyage intergalactique. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=237821.html"><em>VALERIAN</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_4" src="images/Valerian.jpg" class="mad_max" alt="affiche du film Valérian Et La Cité Des Mille Planètes">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         Dans "Alien, le huitième passager", l'équipage du vaisseau spatial Nostromo répond à un signal de détresse sur une planète inconnue. Ils découvrent un vaisseau extraterrestre où l'un des leurs est infecté par une forme de vie alien hostile. Rapidement, l'équipage se retrouve traqué par une créature terrifiante qui se développe à bord du vaisseau. Les membres doivent lutter pour leur survie alors que le xenomorphe démontre une intelligence redoutable et une force inarrêtable.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=62.html"><em>ALIEN</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_5" src="images/alien.jpg" class="mad_max" alt="affiche du film Alien">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
         Dans "District 9", des extraterrestres, surnommés "Prawns", ont échoué sur Terre et sont forcés de vivre dans un quartier ghettoisé à Johannesburg. Lorsqu'un employé gouvernemental, Wikus van de Merwe, est accidentellement exposé à une substance extraterrestre, il commence à subir des mutations. Traqué par son propre gouvernement, Wikus se tourne vers les Prawns pour obtenir de l'aide.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=143026.html"><em>DISTRICT 9</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_6" src="images/district9.jpg" class="mad_max" alt="affiche du film District 9">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
        Dans "Avatar", un ancien marine paraplégique nommé Jake Sully est recruté pour participer au programme Avatar sur la planète Pandora. Là-bas, il prend le contrôle d'un avatar Na'vi, une race indigène de Pandora, dans le but d'interagir pacifiquement avec eux et de sécuriser l'accès à une précieuse ressource, l'unobtanium. Cependant, au fur et à mesure que Jake s'immerge dans la culture Na'vi, il remet en question les motivations de l'entreprise minière et se rallie finalement à leur cause.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=61282.html"><em>AVATAR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_7" src="images/avatar.jpg" class="mad_max" alt="affiche du film Avatar">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
        Dans "Moon", un homme nommé Sam Bell travaille seul dans une station minière sur la Lune, extrayant de l'hélium-3 pour la Terre. Alors qu'il se prépare à rentrer chez lui après un contrat de trois ans, il commence à faire des découvertes troublantes sur la véritable nature de sa mission et de sa propre existence. Confronté à des clones de lui-même et à des révélations choquantes, Sam doit remettre en question sa réalité et l'objectif de sa présence sur la Lune.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=136189.html"><em>MOON</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_8" src="images/moon.jpg" class="mad_max" alt="affiche du film Moon">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
       Dans "Planète rouge", une équipe de scientifiques est envoyée sur Mars pour enquêter sur la disparition mystérieuse d'une colonie minière. Alors qu'ils explorent la planète rouge, ils découvrent des indices troublants et affrontent des dangers mortels, y compris des robots autonomes et des créatures extraterrestres hostiles. Leur mission se transforme rapidement en une lutte pour la survie alors qu'ils luttent contre des forces inconnues et des trahisons au sein de leur propre équipe.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=27268.html"><em>PLANETE ROUGE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_10" src="images/PlaneteRouge.jpg" class="batman" alt="affiche du film Planète Rouge">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume4">
       Dans "Thor : Ragnarok", Thor se retrouve emprisonné sur la planète Sakaar, dirigée par le Grand Maître, où il est forcé de combattre comme gladiateur. Pendant ce temps, sa sœur aînée, Hela, déchaîne le Ragnarok, menaçant la destruction d'Asgard. Thor doit s'échapper de Sakaar, retrouver son marteau Mjöllnir et unir ses forces avec Hulk, Loki et d'autres alliés improbables pour arrêter Hela et sauver Asgard.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=223252.html"><em>THOR: RAGNAROK</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_11" src="images/thor.jpg" class="batman" alt="affiche du film Thor: Ragnarok">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
       Dans "Jupiter : Le Destin de l'univers", Jupiter Jones, une jeune femme ordinaire de la Terre, découvre qu'elle est l'héritière d'une lignée extraterrestre puissante. Elle se retrouve alors plongée au cœur d'une lutte pour le contrôle de l'univers entre des familles royales intergalactiques. Aidée par Caine Wise, un guerrier génétiquement modifié, Jupiter doit maîtriser ses pouvoirs et protéger l'avenir de l'humanité. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=198721.html"><em>LE DESTIN DE L'UNIVERS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_12" src="images/jupiter.jpg" class="edge" alt="affiche du film Jupiter: le destin de l'univers">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
      Dans "Pitch Black", un groupe de voyageurs se retrouve échoué sur une planète désolée après le crash de leur vaisseau spatial. Ils découvrent rapidement que la planète est habitée par des créatures extraterrestres mortelles qui surgissent des ténèbres pour les chasser. Alors qu'ils tentent de survivre dans ce monde hostile, les membres du groupe doivent faire face à leurs propres peurs et rivalités internes.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=25132.html"><em>PITCH BLACK</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_14" src="images/pitch.jpg" class="edge" alt="affiche du film Pitch Black">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
      Dans "Interstellar", alors que la Terre est menacée par une famine mondiale due à la dégradation de l'environnement, un groupe d'astronautes est envoyé dans l'espace à la recherche d'une nouvelle planète habitable. Guidés par des phénomènes cosmiques mystérieux, ils explorent des trous de ver pour voyager vers des systèmes stellaires lointains. Leur voyage met à l'épreuve leur endurance physique et mentale, ainsi que leurs liens émotionnels avec ceux qu'ils ont laissés derrière eux.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=114782.html"><em>INTERSTELLAR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_15" src="images/interstellar.jpg" class="edge" alt="affiche du film Interstellar">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
      Dans "Seul sur Mars", un astronaute nommé Mark Watney est laissé pour mort lors d'une mission sur Mars après une tempête. Il se retrouve seul sur la planète rouge, avec des ressources limitées et aucune communication avec la Terre. Déterminé à survivre, Watney utilise son ingéniosité pour cultiver de la nourriture, réparer son habitat et communiquer avec la NASA. Pendant ce temps, l'agence spatiale et ses coéquipiers planifient une mission de sauvetage risquée pour le ramener sur Terre. 
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=221524.html"><em>SEUL SUR MARS</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_16" src="images/seulsurmars.jpg" class="batman" alt="affiche du film Seul Sur Mars">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Terminator", un cyborg indestructible, envoyé du futur, traque Sarah Connor, cible d'un assassinat destiné à altérer le cours de l'histoire. Avec l'aide de Kyle Reese, un soldat du futur, Sarah lutte pour sa survie, réalisant qu'elle détient le destin de l'humanité entre ses mains. Le film offre une combinaison captivante d'action, de suspense et de science-fiction, explorant les thèmes du destin et de la résistance contre la technologie.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=309.html"><em>TERMINATOR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_17" src="images/terminator.jpg" class="terminator" alt="affiche du film Terminator">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume">
          Dans "Upgrade", un homme paralysé accepte une puce implantée dans son cerveau qui lui confère une mobilité retrouvée, mais découvre qu'elle prend le contrôle de son corps pour mener une quête de vengeance. Manipulé par la puce, il se lance dans une série de confrontations violentes avec les responsables de la mort de sa femme. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=252519.html"><em>UPGRADE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_18" src="images/upgrade.jpg" class="terminator" alt="affiche du film Upgrade">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Man of Steel", nous suivons la renaissance de Superman, alias Clark Kent, depuis sa jeunesse jusqu'à sa découverte de son identité et de ses pouvoirs exceptionnels. Alors qu'il se dévoile au monde, il doit affronter les conséquences de ses pouvoirs sur Terre et se dresser contre le redoutable général Zod, un ancien compatriote kryptonien déterminé à conquérir la planète. Il fera l'apprentissage de ses origines et de son rôle en tant que sauveur de l'humanité. 
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=123348.html"><em>MAN OF STEEL</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_19" src="images/man_of_steel.jpg" class="terminator" alt="affiche du film Man Of Steel">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
          Dans "Chronicle", trois lycéens découvrent un mystérieux objet qui leur confère des pouvoirs télékinétiques. Ils commencent à explorer et à utiliser leurs nouveaux pouvoirs pour leurs propres gains personnels, mais rapidement, leurs actions prennent une tournure sombre et dangereuse. Alors que leur amitié est mise à l'épreuve, ils doivent faire face aux conséquences de leurs actes et à la corruption de leurs pouvoirs.
        </p>
      </div>
      <div><h2><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=184617.html"><em>CHRONICLE</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_20" src="images/chronicle.jpg" class="terminator" alt="affiche du film Chronicle">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume3">
          Dans "La Guerre des mondes", Tom Cruise incarne un père tentant de protéger désespérément ses enfants lors d'une invasion extraterrestre brutale et impitoyable. Alors que la Terre est plongée dans le chaos, la famille doit naviguer à travers des paysages dévastés et affronter des créatures terrifiantes pour trouver refuge. Le film explore les thèmes de la survie, de la famille et de la résilience humaine face à une menace existentielle.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/series/ficheserie_gen_cserie=23873.html"><em>lA GUERRE DES MONDES</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_21" src="images/La_Guerre_des_mondes.jpg" class="gardiens" alt="affiche du film La Guerre Des Mondes">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         Dans "The Darkest Hour", un groupe d'amis américains se retrouve à Moscou lors d'une invasion extraterrestre soudaine. Les envahisseurs prennent la forme d'une énergie invisible, rendant la lutte contre eux particulièrement difficile. Les survivants doivent trouver des moyens ingénieux pour éviter la détection des aliens et tenter de s'échapper de la ville dévastée.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=179924.html"><em>THE DARKEST HOUR</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_22" src="images/darkest_hour.jpg" class="batman" alt="affiche du film The Darkest Hour">
      </div>
    </div>
    
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    <br><br><br<<br><br>
    
    <div class="container">
      <div class="text-container">
        <p class="resume2">
         Dans "John Carter", un ancien soldat de la guerre civile américaine se retrouve mystérieusement transporté sur la planète Mars. Là, il découvre un monde en conflit, où différentes factions et espèces extraterrestres se disputent le contrôle. Carter se retrouve impliqué dans les luttes politiques et militaires de ce monde, utilisant ses compétences de combattant pour aider les habitants de Mars.
        </p>
      </div>
      <div><h2 class="h2"><a href="https://www.allocine.fr/film/fichefilm_gen_cfilm=137263.html"><em>JOHN CARTER</em></a></h2></div>
      <div class="image-container">
        <img id="ancre_24" src="images/john_carter.jpg" class="terminator" alt="affiche du film John Carter">
      </div>
    </div>
    
   </div>
   
   <div class="overlay"></div>
    
</body>
</html>""" 
print(html)
