
import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title>Page Horreur</title>
    <link rel="stylesheet"  href="Style/style_horreur.css">
</head>
<body>
    <div class="content">
     <div class="largeur_texte"><h1>FILM D'HORREUR</h1></div>
      
     <div class="largeur_texte"><p style="text-decoration: underline;"><strong>Plongez dans l'obscurité avec le Film d'Horreur</strong></p></div><br>
     
     <div class="largeur_texte">
      <p>Les films d'horreur sont bien plus qu'une simple expérience cinématographique : ce sont des voyages captivants vers l'inconnu, des frissons qui parcourent votre échine et des histoires qui hantent vos rêves les plus profonds.</p>
     </div>
     <br>
     
     <div class="largeur_texte">
      <p>Imaginez-vous sur le bord de votre siège, votre cœur battant la chamade, alors que chaque ombre semble cacher un danger invisible. Les films d'horreur vous transportent dans des mondes où le surnaturel se mêle au réel, où chaque bruit est une menace potentielle, et où chaque coin sombre recèle un mystère terrifiant.</p>
     </div>
     <br>
      
     <div class="largeur_texte">
      <form method="post" action="redirect_horreur.py" id="actionForm1">
     </div>
       
     <div class="largeur_texte"><p style="text-decoration: underline;">A quel point voulez--vous avoir peur?</p></div>
     
     <div id="scene">
      <input type="radio" name="horreur1" value="point1" id="point1">
      <label for="point1">Pas trop, je suis peureux(se)</label><br>
      <input type="radio" name="horreur1" value="point2" id="point2">
      <label for="point2">Beaucoup!</label><br>
     </div>
     
     <div class="largeur_texte"><p style="text-decoration: underline;">Quel type d'horreur préférez-vous ?</p></div>
      
     <div id="scene">
     
      <input type="radio" name="horreur2" value="type1" id="type1">
      <label for="type1">Horreur psychologique</label><br>
      <input type="radio" name="horreur2" value="type2" id="type2">
      <label for="type2">Horreur surnaturel</label><br>
      <input type="radio" name="horreur2" value="type3" id="type3">
      <label for="type3">Films de monstres</label>
      
     </div><br>
      
     <div class="largeur_texte"><p style="text-decoration: underline;">Quels éléments vous effraient le plus dans un film d'horreur ?</p></div>
      
     <div id="scene">
     
      <input type="radio" name="horreur3" value="peur1" id="peur1">
      <label for="peur1">Les sauts d'effroi</label><br>
      <input type="radio" name="horreur3" value="peur2" id="peur2">
      <label for="peur2">Les scènes sanglantes</label><br>
      <input type="radio" name="horreur3" value="peur3" id="peur3">
      <label for="peur3">Les événements surnaturels</label>
      
     </div><br>
      
     <div><button type="submit" value="suivant">Movie Advice</button></div>
     
     </form>
    </div>

    <div class="overlay"></div>
    
</body>
</html>""" 
print(html)
    
    