
import cgi
import urllib.parse

print("Content-type: text/html; charset=UTF-8")
print()

# Récupération des données postées par le formulaire
form = cgi.FieldStorage()

# Extraction des choix de l'utilisateur
horreur1 = form.getvalue('horreur1')
horreur2 = form.getvalue('horreur2')
horreur3 = form.getvalue('horreur3')

# Déterminer l'ancre en fonction des choix de l'utilisateur
try:
    if horreur1 == 'point1' and horreur2 == 'type1' and horreur3 == 'peur1': # HEREDITARY
        ancre = 'ancre_1'
    elif horreur1 == 'point1' and horreur2 == 'type1' and horreur3 == 'peur2': # SHUTTER ISLAND
        ancre = 'ancre_2'
    elif horreur1 == 'point1' and horreur2 == 'type1' and horreur3 == 'peur3': # THE SIXTH SENS
        ancre = 'ancre_3'
    elif horreur1 == 'point1' and horreur2 == 'type2' and horreur3 == 'peur1': # Insidious
        ancre = 'ancre_4' 
    elif horreur1 == 'point1' and horreur2 == 'type2' and horreur3 == 'peur2': # EVIL DEAD
        ancre = 'ancre_5'
    elif horreur1 == 'point1' and horreur2 == 'type2' and horreur3 == 'peur3': # L'ORPHELINAT
        ancre = 'ancre_6'
    elif horreur1 == 'point1' and horreur2 == 'type3' and horreur3 == 'peur1': # Cloverfield
        ancre = 'ancre_7'
    elif horreur1 == 'point1' and horreur2 == 'type3' and horreur3 == 'peur2': # Tremors
        ancre = 'ancre_8'
    elif horreur1 == 'point1' and horreur2 == 'type3' and horreur3 == 'peur3': #The Ritual
        ancre = 'ancre_9'
    elif horreur1 == 'point2' and horreur2 == 'type1' and horreur3 == 'peur1': # SINISTER
        ancre = 'ancre_10'
    elif horreur1 == 'point2' and horreur2 == 'type1' and horreur3 == 'peur2': # HEREDITARY
        ancre = 'ancre_1'
    elif horreur1 == 'point2' and horreur2 == 'type1' and horreur3 == 'peur3': # L'Exorcisme d'Emily Rose
        ancre = 'ancre_12'
    elif horreur1 == 'point2' and horreur2 == 'type2' and horreur3 == 'peur1': # Dans le noir
        ancre = 'ancre_13' 
    elif horreur1 == 'point2' and horreur2 == 'type2' and horreur3 == 'peur2': # La Nonne
        ancre = 'ancre_14'
    elif horreur1 == 'point2' and horreur2 == 'type2' and horreur3 == 'peur3': # L'Exorciste
        ancre = 'ancre_15'
    elif horreur1 == 'point2' and horreur2 == 'type3' and horreur3 == 'peur1': # THE DESCENT
        ancre = 'ancre_16'
    elif horreur1 == 'point2' and horreur2 == 'type3' and horreur3 == 'peur2': # LA FORME DE L'EAU
        ancre = 'ancre_17'
    elif horreur1 == 'point2' and horreur2 == 'type3' and horreur3 == 'peur3': # conjuring
        ancre = 'ancre_18'
    else:
        raise Exception
except:
    if horreur1 is None or horreur2 is None or horreur3 is None:
        print("<h1>Revenez en arrière, il vous faut répondre à toutes les questions.</h1>")
else:
    # Construire l'URL de redirection vers la troisième page avec les paramètres du formulaire et l'ancre appropriée
    redirect_url = f"conseil_horreur.py?horreur1={horreur1}&horreur2={horreur2}&horreur3={horreur3}#{ancre}"


#Rediriger l'utilisateur
print(f"<script>window.location.href = '{redirect_url}';</script>")