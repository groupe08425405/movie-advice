# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 08:07:52 2024

@author: hdubois
"""

import cgi
print("Content-type: text/html; charset=UTF-8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
    <title>Film de science-fiction</title>
    <link rel="stylesheet"  href="Style/style_sci-fiction.css">	
</head>
<body>
    
   <div class="content">
     <div class="largeur_texte"><h1>FILM DE SCIENCE-FICTION</h1></div>
     
     <div class="largeur_texte">
      <p><strong>Explorez des mondes inconnus et des horizons infinis avec notre dernier chef-d'œuvre de science-fiction !</strong></p>
      <p>Plongez dans un voyage épique à travers l'espace et le temps, où chaque planète cache des mystères et des dangers inattendus. Découvrez des civilisations extraterrestres, des technologies futuristes et des phénomènes cosmiques éblouissants qui vous transporteront au-delà de votre imagination.</p>
      <p>Avec des effets spéciaux révolutionnaires et une histoire captivante, notre film vous entraîne dans une aventure palpitante où le destin de l'humanité est en jeu. Préparez-vous à vivre une expérience cinématographique sensationnelle qui repousse les frontières de l'exploration spatiale ! </p>
     </div><br>
     
    <div class="largeur_texte">
     <form method="post" action="redirect_sci-fiction.py" id="actionForm1">
     <p>OU DEVRAIT SE DEROULER L'HISTOIRE?:</p>
    </div>
    
    <div id="scene">
    
     <input type="radio" name="science1" value="lieu1" id="lieu1">
     <label for="lieu1">Dans l'espace</label><br>
     <input type="radio" name="science1" value="lieu2" id="lieu2">
     <label for="lieu2">Sur une autre planète</label><br>
     <input type="radio" name="science1" value="lieu3" id="lieu3">
     <label for="lieu3">Sur Terre</label><br>
    
    </div><br>

    <div class="largeur_texte"><p>QUEL CONCEPT VOUS INTERESSE?:</p></div>
    
    <div id="scene">
    
     <input type="radio" name="science2" value="concept1" id="robots">
     <label for="robots">Robots, machine, cyborg</label><br>
     <input type="radio" name="science2" value="concept2" id="héros">
     <label for="héros">Super-héros</label><br>
     <input type="radio" name="science2" value="concept3" id="alien">
     <label for="alien">Extraterrestres, aliens...</label><br>
     <input type="radio" name="science2" value="concept4" id="exo_planète">
     <label for="exo_planète">L'histoire se déroule sur une autre planète</label><br>
     
    </div><br>
    
    <div class="largeur_texte"><p>VOULEZ-VOUS FAIRE DES DECOUVERTES?:</p></div>
    
    <div id="scene">
    
     <input type="radio" name="science3" value="populaire" id="populaire">
     <label for="célèbre">Film Célèbre</label><br>
     <input type="radio" name="science3" value="impopulaire" id="impopulaire">
     <label for="inconnu">Film Moins Connu</label><br>
     
    </div><br>
    
    <button type="submit" value="suivant">Movie Advice</button>
     
    </form>
   </div>

   <div class="overlay"></div>
    
</body>
</html>""" 
print(html)
    