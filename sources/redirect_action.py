
import cgi
import urllib.parse

print("Content-type: text/html; charset=UTF-8")
print()

# Récupération des données postées par le formulaire
form = cgi.FieldStorage()

# Extraction des choix de l'utilisateur
action1 = form.getvalue('action1')
action2 = form.getvalue('action2')
action3 = form.getvalue('action3')

# Déterminer l'ancre en fonction des choix de l'utilisateur
try:
    if action1 == 'scene1' and action2 == 'contexte1' and action3 == 'GIRL':
        ancre = 'ancre_1'
    elif action1 == 'scene1' and action2 == 'contexte1' and action3 == 'BOY':
        ancre = 'ancre_2'
    elif action1 == 'scene1' and action2 == 'contexte2' and action3 == 'GIRL':
        ancre = 'ancre_3'
    elif action1 == 'scene1' and action2 == 'contexte2' and action3 == 'BOY':
        ancre = 'ancre_4'
    elif action1 == 'scene2' and action2 == 'contexte1' and action3 == 'GIRL':
        ancre = 'ancre_5'
    elif action1 == 'scene2' and action2 == 'contexte1' and action3 == 'BOY':
        ancre = 'ancre_6'
    elif action1 == 'scene2' and action2 == 'contexte2' and action3 == 'GIRL':
        ancre = 'ancre_7'
    elif action1 == 'scene2' and action2 == 'contexte2' and action3 == 'BOY':
        ancre = 'ancre_8'
    elif action1 == 'scene3' and action2 == 'contexte1' and action3 == 'GIRL':
        ancre = 'ancre_9'
    elif action1 == 'scene3' and action2 == 'contexte1' and action3 == 'BOY':
        ancre = 'ancre_10'
    elif action1 == 'scene3' and action2 == 'contexte2' and action3 == 'GIRL':
        ancre = 'ancre_11'
    elif action1 == 'scene3' and action2 == 'contexte2' and action3 == 'BOY':
        ancre = 'ancre_8'
    elif action1 == 'scene4' and action2 == 'contexte1' and action3 == 'GIRL':
        ancre = 'ancre_13'
    elif action1 == 'scene4' and action2 == 'contexte1' and action3 == 'BOY':
        ancre = 'ancre_14'
    elif action1 == 'scene4' and action2 == 'contexte2' and action3 == 'GIRL':
        ancre = 'ancre_15'
    elif action1 == 'scene4' and action2 == 'contexte2' and action3 == 'BOY':
        ancre = 'ancre_16'
    elif action1 == 'scene5' and action2 == 'contexte1' and action3 == 'GIRL':
        ancre = 'ancre_17'
    elif action1 == 'scene5' and action2 == 'contexte1' and action3 == 'BOY':
        ancre = 'ancre_18'
    elif action1 == 'scene5' and action2 == 'contexte2' and action3 == 'GIRL':
        ancre = 'ancre_19'
    elif action1 == 'scene5' and action2 == 'contexte2' and action3 == 'BOY':
        ancre = 'ancre_20'
    else:
        raise Exception
except:
    if action1 is None or action2 is None or action3 is None:
        print("<h1>Revenez en arrière, il vous faut répondre à toutes les questions.</h1>")
else:
    # Construire l'URL de redirection vers la troisième page avec les paramètres du formulaire et l'ancre appropriée
    redirect_url = f"conseil_action.py?action1={action1}&action2={action2}&action3={action3}#{ancre}"


#Rediriger l'utilisateur
print(f"<script>window.location.href = '{redirect_url}';</script>")
